const colors = require('colors');
const _ = require('lodash');

const beerService = require('../services/beer.service');
const pokemonService = require('../services/pokemon.service');

async function beersExample() {
  console.info("Beers Example".underline);

  try {
    const [ firstBeer, ...others ] = await beerService.findAll();
    console.log(`The first beer's name is "${firstBeer.name}"\n`.blue.bold);
  } catch(e) {
    console.error('[beersExample]', e);
  }
}

async function randomExample() {
  console.info("Random Example".underline);

  try {
    const [ randomBeer, randomPokemon ] = await Promise.all([beerService.getRandom(), pokemonService.getRandom()]);
    const status = `The pokemon ${_.capitalize(randomPokemon.name)} is drinking a ${_.capitalize(randomBeer.name)}`;

    console.log(status.italic.white.bgBlack);
  } catch(e) {
    console.error('[randomExample]', e);
  }
}

module.exports = { beersExample, randomExample };