class Service {

    log(message, ...elements) {
        console.log(`[${this.constructor.name}#${message}]`, ...elements);
    }

}


module.exports = Service;