const _ = require('lodash');

const { beersExample, randomExample } = require('./some_examples/async');
const { timer } = require('./some_examples/observables');
const { numberGenerator, logGenerator, otherGenerator } = require('./some_examples/generators');
const errorHandler = require('./utils/errorHandler');

async function triggerAsyncAwait() {
  await beersExample();
  await randomExample();
}

function triggerObservable() {
  const subscriber = timer.subscribe(
    (iteration) => {
      console.log(iteration);
      
      if(_.endsWith(iteration, '3')) {
        subscriber.unsubscribe()
      }
    },
    (err) => console.errror(err)
  );
}

function triggerErrorHandling() {
  try {
    Your_Mama
  } catch(e) {
    errorHandler.log("triggerErrorHandling", e);
    errorHandler.saveInDB("triggerErrorHandling", e);
    errorHandler.storeInFile("triggerErrorHandling", e);
    errorHandler.pushToQueue("triggerErrorHandling", e);
  }
}

function triggerGenerators() {
  const genNumber = numberGenerator();
  let numberDone = false;

  while(!numberDone) {
    const numberIteration = genNumber.next();
    numberDone = numberIteration.done;

    console.log(numberIteration);
  }

  console.log("\n");

  const elements = ['Hello', 'Friend'];
  const genLogger = logGenerator(elements);
  
  genLogger.next(); // execute function until first yield is accountered
  elements.forEach(e => genLogger.next(e));

  console.log("\n");

  const genOther = otherGenerator();
  let otherDone = false;
  
  while(!otherDone) {
    const otherIteration = genOther.next();
    otherDone = otherIteration.done;

    console.log(otherIteration);
  }
}

triggerAsyncAwait();
//triggerObservable();
//triggerErrorHandling();
//triggerGenerators();