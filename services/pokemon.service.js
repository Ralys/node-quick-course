const Service = require('./service');

const axios = require('axios');

class PokemonService extends Service {

    getRandom() {
        this.log('getRandom');

        const randomNumber = parseInt(Math.random() * 50);
        const url = `http://pokeapi.co/api/v2/pokemon/${randomNumber}`;
        return axios.get(url).then(response => response.data);
    }

}

module.exports = new PokemonService();