const Service = require('./service');

const axios = require('axios');

class BeerService extends Service {
    
    findAll() {
        this.log('findAll');
        return axios.get('https://api.punkapi.com/v2/beers').then(response => response.data);
    }

    getRandom() {
        this.log('getRandom');
        return axios.get('https://api.punkapi.com/v2/beers/random').then(response => response.data[0]);
    }
    
}

module.exports = new BeerService();