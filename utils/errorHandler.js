const moment = require('moment');
const colors = require('colors');

const mongoConfig = require('../config/db');
const storageConfig = require('../config/storage');
const rabbitMqConfig = require('../config/queue');

const { MongoClient } = require('mongodb');
const amqp = require('amqp-simple-connect');
const fs = require('fs-extra');

const now = () => moment().format('DD/MM/YYYY HH:mm:ss');

const formatError = (tag, error) => {
    return {
        date: moment().format(),
        formattedDate: now(),
        tag: tag,
        message: error.message,
        stacktrace: error.stack
    };
};

class ErrorHandler {

    log(tag, error) {
        console.error(`${now()} [${tag}]: ${error.message}`.red);
    }

    async saveInDB(tag, error) {
        const formattedError = formatError(tag, error);
        const url = `mongodb://${mongoConfig.host}:${mongoConfig.port}/${mongoConfig.database}`;
        
        try {
            const db = await MongoClient.connect(url);
            db.collection('errors').insert(formattedError);
            db.close();
        } catch(e) {
            this.log('saveInDB', e);
        }
    }

    async storeInFile(tag, error) {
        const formattedError = formatError(tag, error);
        const { fileName } = storageConfig;
        
        try {
            await fs.ensureFile(fileName);
            const errors = await fs.readJson(fileName, { throws: false });
            const newErrors = errors ? errors.concat(formattedError) : [ formattedError ];
            await fs.writeJson(fileName, newErrors);
        } catch(e) {
            this.log('storeInFile', e);
        }
    }

    async pushToQueue(tag, error) {
        const formattedError = formatError(tag, error);
        const { path, exchange, queueName } = rabbitMqConfig;
        
        try {
            const wrapper = await amqp.connect({ path, exchange });
            const queue = await amqp.createQueue(queueName);
            amqp.publish(queueName, formattedError);
            await queue.close();
            wrapper.connection.close();
        } catch(e) {
            this.log('pushToQueue', e);
        }
    }

}

module.exports = new ErrorHandler();