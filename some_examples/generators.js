function* numberGenerator() {
    for(let number = 0; number < 10; number++) {
        yield number;
    }
}

function* logGenerator(elements) {
    //elements.forEach(() => console.log(yield)); // DOES NOT WORK BECAUSE CONTEXT CHANGE

    for(let i in elements) {
        console.log(yield);
    }
}

function* otherGenerator() {
    yield "call to numberGenerator next";
    yield* numberGenerator();
    yield "the end";
}


module.exports = { numberGenerator, logGenerator, otherGenerator };