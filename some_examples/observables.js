const { Observable } = require('rxjs/Rx');

module.exports = {
    timer: Observable.timer(0, 1000).map(second => `Second #${second}`)
};
